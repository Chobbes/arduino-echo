"""
  Copyright (C) 2013 Calvin Beck

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

"""

import serial
import time
import sys
import os

# Need to find the home directory on your computer - it differs.
home = os.path.expanduser('~')

# Our toolchain stores the port that the Arduino is on in a port file.
port_file_path = home + '/.arduino_port_0'

# We need to read the location of the port that we want to talk to!
# It's probably something like /dev/ttyACM0 or something on Ubuntu.
port_file = open(port_file_path, 'r')
port_loc = port_file.readline().strip()  # Strip to get rid of newlines

# Now we need to open the serial port to talk to the Arduino!
ard_serial = serial.Serial(port_loc, 9600)
time.sleep(2) # Need to wait a bit for everything to get initialized.

# Let's say hello to the Arduino and then echo everything it says to us!
ard_serial.write(bytes("Hello Arduino!\n", encoding='ascii'))

# We will switch case every character :).
upper = True

while True:
    upper = not upper

    # Fetch a byte from the Arduino
    ard_byte = ard_serial.read()

    # Decode the byte.
    char = ard_byte.decode('utf8')

    # Print the character (don't print a newline after it)
    print(char, end='')

    # Send back the character, but change the case.
    if upper:
        ard_serial.write(ard_byte.upper())
    else:
        ard_serial.write(ard_byte.lower())
